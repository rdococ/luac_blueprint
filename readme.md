luac_blueprint
==============

The luac_blueprint mod for Minetest allows you to easily copy and paste code between luacontrollers. It also allows you to distribute code fairly easily. Left-click on a luacontroller to copy its code to the blueprint, and shift + right-click on one to paste code to the luacontroller. If you right-click on anything that isn't a luacontroller, a formspec showing the code in the blueprint will appear. Area protection is obeyed but only while pasting.

If you click on a luacontroller while the blueprint has code on it, that code will be overwritten, so be careful. Similarly with shift+right-clicking a luacontroller with code on it. To help you remember, think that a left-click is like you're mining the code so that it will be in your hand, and a shift+right-click is like placing it.