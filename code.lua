local display_formspec = function (itemstack, user)
	-- On right-clicking the air: Show the blueprint's code.
	local meta = itemstack:get_meta()
	
	minetest.show_formspec(user:get_player_name(), "luac_blueprint:luac_blueprint_code", [[
		size[8,9]
		label[0,0;]] .. minetest.formspec_escape("Code") .. [[]
		textlist[0,0.5;7.775,8.5;code;]] .. minetest.formspec_escape(meta:get_string("code")):gsub("\n", ",") .. [[
	]])
end

local copy_from_luac = function (itemstack, user, pointed_thing)
	-- On left-click: Take code from the luacontroller and impart it to the blueprint.
	local blueprint_meta = itemstack:get_meta()
	
	if not pointed_thing.under then return end
	local pos = pointed_thing.under
	local node = minetest.get_node(pos)
	
	if not node then return end
	if node.name:sub(1, ("mesecons_luacontroller"):len()) ~= "mesecons_luacontroller" then
		return display_formspec(itemstack, user, pointed_thing)
	end
	
	local node_meta = minetest.get_meta(pos)
	
	blueprint_meta:set_string("code", node_meta:get_string("code"))
	minetest.chat_send_player(user:get_player_name(), "Copied from luacontroller.")
	
	return itemstack
end
local paste_to_luac = function (itemstack, user, pointed_thing)
	-- On shift+right-click: Impart the code from the blueprint to the luacontroller.
	local blueprint_meta = itemstack:get_meta()
	
	if not pointed_thing.under then return end
	local pos = pointed_thing.under
	local node = minetest.get_node(pos)
	
	if not node then return end
	if node.name:sub(1, ("mesecons_luacontroller"):len()) ~= "mesecons_luacontroller" then
		return display_formspec(itemstack, user, pointed_thing)
	end
	local node_meta = minetest.get_meta(pos)
	
	local name = user:get_player_name()
	if minetest.is_protected(pos, name) then
		minetest.record_protection_violation(pos, name)
		return
	end
	
	local code = blueprint_meta:get_string("code")
	
	node_meta:set_string("code", code)
	node_meta:set_string("formspec", "size[12,10]"..
		"background[-0.2,-0.25;12.4,10.75;jeija_luac_background.png]"..
		"textarea[0.2,0.2;12.2,9.5;code;;"..code.."]"..
		"image_button[4.75,8.75;2.5,1;jeija_luac_runbutton.png;program;]"..
		"image_button_exit[11.72,-0.25;0.425,0.4;jeija_close_window.png;exit;]"..
		"label[0.1,9;".."pasted from blueprint".."]")
	
	minetest.chat_send_player(user:get_player_name(), "Pasted to luacontroller.")
end

minetest.register_alias("luac_blueprint:luac_blueprint_copyonce", "luac_blueprint:luac_blueprint")
minetest.register_alias("luac_blueprint:luac_blueprint_pasteonly", "luac_blueprint:luac_blueprint")
minetest.register_alias("luac_blueprint:luac_blueprint_reusable", "luac_blueprint:luac_blueprint")

minetest.register_alias("luac_blueprint", "luac_blueprint:luac_blueprint")

minetest.register_craftitem("luac_blueprint:luac_blueprint", {
	description = "LuaController blueprint",
	
	inventory_image = "luac_blueprint.png",
	wield_image = "luac_blueprint.png",
	
	on_use = copy_from_luac,
	on_place = paste_to_luac,
	on_secondary_use = display_formspec
})

local silicon = "mesecons_materials:silicon"
local dye_blue = "dye:blue"
minetest.register_craft({
	output = "luac_blueprint",
	recipe = {
		{"", dye_blue, ""},
		{dye_blue, silicon, dye_blue},
		{"", dye_blue, ""}
	}
})